﻿using EGIC.Core.Domain.RamadanBags;
using EGIC.Web.Framework;
using System;
using System.Collections.Generic;

namespace RamadanBags.Services.RamadanBags
{
    public interface IRamadanBagService
    {

        #region Picture IO

        bool DeletePictureOnFileSystem(RamadanBag bag);

        bool DeletePictureOnFileSystem(string picturePath);

        string SavePictureInFile(byte[] pictureBinary);

        byte[] LoadPictureBinary(RamadanBag bag);

        byte[] LoadPictureFromFile(string ID);

        #endregion

        #region Pictures

        /// <summary>
        /// delete Picture from db
        /// </summary>
        /// <param name="Picture">Picture to be deleted</param>
        bool DeleteRamadanBag(RamadanBag bag);

        /// <summary>
        /// Add new Picture to LP_Pictures table
        /// </summary>
        /// <param name="Picture"></param>
        RamadanBag InsertRamadanBag(RamadanBag bag, byte[] pictureBinary);

        /// <summary>
        /// Add new Picture to LP_Pictures table
        /// </summary>
        /// <param name="Picture"></param>
        /// <returns>Saved successfully or not</returns>
        RamadanBag InsertRamadanBag(RamadanBag bag);

        /// <summary>
        /// Update an existing Picture in LP_Pictures table
        /// </summary>
        /// <param name="picture"></param>
        void SaveRamadanBag(RamadanBag bag);

        /// <summary>
        /// get Picture by Picture code
        /// </summary>
        /// <param name="code">code to search by</param>
        /// <returns>Picture</returns>
        RamadanBag GetRamadanBagByID(string ID);

        /// <summary>
        /// Gets all Pictures
        /// </summary>
        /// <param name="ID">ID; null to load all records</param>
        /// <param name="serial">serial; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>list of Pictures</returns>
        IPagedList<RamadanBag> GetAllRamadanBags(
            out string errorMessage,
            string username = null,
            string ID = null,
            string serial = null,
            int pageIndex = 0,
            int pageSize = int.MaxValue
            );

        /// <summary>
        /// Gets the default picture URL
        /// </summary>
        /// <returns>Picture URL</returns>
        string GetDefaultPictureUrl();

        
        #endregion

    }
}