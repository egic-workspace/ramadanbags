﻿using EGIC.Core.Domain.RamadanBags;
using EGIC.Data.DAL;
using EGIC.Web.Framework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RamadanBags.Services.RamadanBags
{
    public class RamadanBagService : IRamadanBagService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region CTOR

        public RamadanBagService(IServiceProvider service, IHostingEnvironment hostingEnvironment)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
            _hostingEnvironment = hostingEnvironment;
        }

        #endregion

        #region Ramadan Bag Utilities
        private string PrepareInsertCommand(RamadanBag bag)
        {
            return $"INSERT INTO SV_BAG_ISSUE (SAL_CODE, BENF_NAME, ID_NO, GOV_CODE, CITY_CODE, IMG_PATH1,IMG_PATH2, JOB, FAMILY, MOBILE,SERIAL,USERNAME) VALUES ({bag.ShopCode}, '{bag.Name}', '{bag.ID}', {bag.GovCode}, {bag.CityCode}, '{bag.ID_FrontImagePath}', '{bag.ID_RearImagePath}', '{bag.Job}', {bag.FamilyCount}, '{bag.Mobile}', '{bag.Serial}','{bag.Username}')";
        }
        private string SelectRamadanBages()
        {
            return @"SELECT B.SAL_CODE,S.TRAD_NAME SHOP_NAME,S.SUB_AREA, B.BENF_NAME, B.ID_NO, B.GOV_CODE, B.CITY_CODE, B.IMG_PATH1,B.IMG_PATH2, B.JOB, B.FAMILY, B.MOBILE,B.SERIAL,B.USERNAME,B.REC_DATE 
                                FROM SV_BAG_ISSUE B
                                INNER JOIN SV_SHOPS S ON S.SAL_CODE = B.SAL_CODE";
        }
        private void UpdatePicturePath(string ID, string IDFrontNewPath, string IDRearNewPath)
        {
            if (string.IsNullOrWhiteSpace(ID))
                throw new ArgumentNullException(nameof(ID));

            if (string.IsNullOrWhiteSpace(IDFrontNewPath))
                throw new ArgumentNullException(nameof(IDFrontNewPath));
            IDFrontNewPath = IDFrontNewPath.Replace("\\", "/");
            string updateQuery = $@"UPDATE 
                                    SV_BAG_ISSUE SET
                                        {RamadanBag.IMG_PATH1} = '{IDFrontNewPath}',
                                        {RamadanBag.IMG_PATH2} = '{IDRearNewPath}'
                                    WHERE 
                                        {RamadanBag.ID_NO} = '{ID}'";
            int effectedRows = _oracleAcess.ExecuteNonQuery(updateQuery);
        }

        private string FindImageName()
        {
            string localFilePath = GetPictureLocalPath(string.Empty);
            var filesCount = Directory.GetFiles(localFilePath).Length;
            return $"svfb_ID_{filesCount + 1}.png";
        }

        /// <summary>
        /// Save picture on file system
        /// </summary>
        /// <param name="ID">Picture identifier</param>
        public string SavePictureInFile(byte[] pictureBinary)
        {
            var fileName = FindImageName();
            string localFilePath = GetPictureLocalPath(fileName);
            File.WriteAllBytes(localFilePath, pictureBinary);
            string imgUrl = GetPictureLocalPath(fileName, true).Replace("\\", "/");
            return imgUrl;
        }

        /// <summary>
        /// Gets the loaded picture binary depending on picture storage settings
        /// </summary>
        /// <param name="bag">Picture</param>
        /// <returns>Picture binary</returns>
        public byte[] LoadPictureBinary(RamadanBag bag)
        {
            if (bag == null)
                throw new ArgumentNullException(nameof(bag));

            var result = LoadPictureFromFile(bag.ID);
            return result;
        }

        /// <summary>
        /// Loads a picture from file
        /// </summary>
        /// <param name="ID">Picture identifier</param>
        /// <param name="mimeType">MIME type</param>
        /// <returns>Picture binary</returns>
        public byte[] LoadPictureFromFile(string ID)
        {
            var fileName = $"{ID:0000000}_0.png";
            var filePath = GetPictureLocalPath(fileName);
            if (!File.Exists(filePath))
                return Array.Empty<byte>();
            return File.ReadAllBytes(filePath);
        }

        /// <summary>
        /// Delete a picture on file system
        /// </summary>
        /// <param name="picture">Picture</param>
        public bool DeletePictureOnFileSystem(RamadanBag bag)
        {
            if (bag == null)
                throw new ArgumentNullException(nameof(bag));

            string filePath = _hostingEnvironment.WebRootPath + bag.ID_FrontImagePath;
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Delete a picture on file system
        /// </summary>
        /// <param name="picture">Picture</param>
        public bool DeletePictureOnFileSystem(string picturePath)
        {
            if (string.IsNullOrWhiteSpace(picturePath))
                throw new ArgumentNullException(nameof(picturePath));

            string filePath = _hostingEnvironment.WebRootPath + picturePath;
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get picture local path. Used when images stored on file system (not in the database)
        /// </summary>
        /// <param name="fileName">Filename</param>
        /// <param name="entityType">Entity type</param>
        /// <returns>Local picture path</returns>
        private string GetPictureLocalPath(string fileName, bool get = false)
        {
            string src;
            if (get)
            {
                src = Path.Combine("images/uploaded", fileName);
                src = src.StartsWith("/") ? src : "/" + src;
            }
            else
                src = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploaded", fileName);
            return src;
        }

        /// <summary>
        /// Get picture local path
        /// </summary>
        /// <param name="picture">picture</param>
        /// <param name="getDefaultPicture">get default picture if true</param>
        /// <returns>Local picture path</returns>
        private string GetPictureLocalPath(RamadanBag bag, bool getDefaultPicture = false)
        {
            if (bag == null)
                return getDefaultPicture ? GetDefaultPictureUrl() : null;

            var fileName = $"{bag.ID:0000000}_0.png";
            return GetPictureLocalPath(fileName, true);
        }

        #endregion

        #region Methods

        #region Ramadan Bag

        /// <summary>
        /// Gets the default picture URL
        /// </summary>
        /// <returns>Picture URL</returns>
        public virtual string GetDefaultPictureUrl()
        {

            string defaultImageFileName = "default-image.png";
            var filePath = GetPictureLocalPath(defaultImageFileName);
            if (!File.Exists(filePath))
                return "";
            string src = GetPictureLocalPath(defaultImageFileName, true);
            if (!src.StartsWith("/"))
                src = "/" + src;
            return src;
        }

        /// <summary>
        /// delete Picture from db
        /// </summary>
        /// <param name="Picture">Picture to be deleted</param>
        public virtual bool DeleteRamadanBag(RamadanBag bag)
        {
            if (bag == null)
                throw new ArgumentNullException(nameof(bag));

            if (bag == null)
                return false;
            int effectedRows = 0;
            try
            {
                DeletePictureOnFileSystem(bag);
                string sql = $@"DELETE SV_BAG_ISSUE Where ID_NO = '{bag.ID}'";
                effectedRows = _oracleAcess.ExecuteNonQuery(sql);
            }
            catch
            {
            }
            return effectedRows > 0;
        }

        /// <summary>
        /// Add new Picture to SV_BAG_ISSUE table
        /// </summary>
        /// <param name="Picture"></param>
        public virtual RamadanBag InsertRamadanBag(RamadanBag bag, byte[] pictureBinary)
        {
            if (bag == null)
                throw new ArgumentNullException(nameof(bag));


            string insertCommandText = PrepareInsertCommand(bag);
            int effectedRows = _oracleAcess.ExecuteNonQuery(insertCommandText);
            if (effectedRows > 0)
            {
                SavePictureInFile(pictureBinary);
            }
            return bag;
        }

        /// <summary>
        /// Add new Picture to SV_BAG_ISSUE table
        /// </summary>
        /// <param name="Picture"></param>
        public virtual RamadanBag InsertRamadanBag(RamadanBag bag)
        {
            if (bag == null)
                throw new ArgumentNullException(nameof(bag));

            if (!string.IsNullOrWhiteSpace(bag.ID_FrontImagePath) && bag.ID_FrontImagePath.IndexOf("\\") >= 0)
                bag.ID_FrontImagePath = bag.ID_FrontImagePath.Replace("\\", "/");

            if (!string.IsNullOrWhiteSpace(bag.ID_RearImagePath) && bag.ID_RearImagePath.IndexOf("\\") >= 0)
                bag.ID_RearImagePath = bag.ID_RearImagePath.Replace("\\", "/");

            string insertCommandText = PrepareInsertCommand(bag);

            int effectedRows = _oracleAcess.ExecuteNonQuery(insertCommandText);
            if (effectedRows > 0 && (!string.IsNullOrWhiteSpace(bag.ID_FrontImagePath) || !string.IsNullOrWhiteSpace(bag.ID_RearImagePath)))
            {
                RenamePictureFile(bag);
            }
            return bag;
        }

        private void RenamePictureFile(RamadanBag bag)
        {
            // front
            string finalFrontPictureUrl = "";
            if (!string.IsNullOrWhiteSpace(bag.ID_FrontImagePath))
            {
                string finalFrontImageName = bag.ID_FrontImagePath.Replace("svfb_ID_", $"svfb_{bag.ID}_");
                string finalFrontImagePath = _hostingEnvironment.WebRootPath + GetPictureLocalPath(finalFrontImageName);
                string oldFrontImagePath = _hostingEnvironment.WebRootPath + bag.ID_FrontImagePath;
                File.Move(oldFrontImagePath, finalFrontImagePath);
                finalFrontPictureUrl = GetPictureLocalPath(finalFrontImageName, true).Replace("\\", "/");
            }
            // rear
            string finalRearPictureUrl = "";
            if (!string.IsNullOrWhiteSpace(bag.ID_RearImagePath))
            {
                string finalRearImageName = bag.ID_RearImagePath.Replace("svfb_ID_", $"svfb_{bag.ID}_");
                string finalRearImagePath = _hostingEnvironment.WebRootPath + GetPictureLocalPath(finalRearImageName);
                string oldRearImagePath = _hostingEnvironment.WebRootPath + bag.ID_RearImagePath;
                File.Move(oldRearImagePath, finalRearImagePath);
                finalRearPictureUrl = GetPictureLocalPath(finalRearImageName, true).Replace("\\", "/");
            }
            if (!string.IsNullOrWhiteSpace(bag.ID_FrontImagePath) || !string.IsNullOrWhiteSpace(bag.ID_RearImagePath))
                UpdatePicturePath(bag.ID, finalFrontPictureUrl, finalRearPictureUrl);
        }

        /// <summary>
        /// Update an existing Picture in SV_BAG_ISSUE table
        /// </summary>
        /// <param name="Picture"></param>
        public virtual void SaveRamadanBag(RamadanBag bag)
        {
            if (bag == null)
                throw new ArgumentNullException(nameof(bag));

            if (!string.IsNullOrWhiteSpace(bag.ID_FrontImagePath) && bag.ID_FrontImagePath.IndexOf("\\") >= 0)
                bag.ID_FrontImagePath = bag.ID_FrontImagePath.Replace("\\", "/");

            if (!string.IsNullOrWhiteSpace(bag.ID_RearImagePath) && bag.ID_RearImagePath.IndexOf("\\") >= 0)
                bag.ID_RearImagePath = bag.ID_RearImagePath.Replace("\\", "/");

            string updateQuery = $@"UPDATE 
                                    SV_BAG_ISSUE SET
                                        {RamadanBag.BENF_NAME} = '{bag.Name}',
                                        {RamadanBag.MOBILE} = '{bag.Mobile}',
                                        {RamadanBag.JOB} = '{bag.Job}',
                                        {RamadanBag.CITY_CODE} = '{bag.CityCode}',
                                        {RamadanBag.GOV_CODE} = '{bag.GovCode}',
                                        {RamadanBag.FAMILY} = '{bag.FamilyCount}',
                                        {RamadanBag.SERIAL} = '{bag.Serial}',
                                        {RamadanBag.IMG_PATH1} = '{bag.ID_FrontImagePath}',
                                        {RamadanBag.IMG_PATH2} = '{bag.ID_RearImagePath}',
                                        {RamadanBag.SAL_CODE} = {bag.ShopCode}
                                    WHERE 
                                        {RamadanBag.ID_NO} = '{bag.ID}'";
            int effectedRows = _oracleAcess.ExecuteNonQuery(updateQuery);
        }

        /// <summary>
        /// get Picture by Picture code
        /// </summary>
        /// <param name="ID">code to search by</param>
        /// <returns>Picture</returns>
        public virtual RamadanBag GetRamadanBagByID(string ID)
        {
            if (string.IsNullOrWhiteSpace(ID))
                return null;
            string sqlQuery = $"{SelectRamadanBages()} WHERE B.{RamadanBag.ID_NO} = '{ID}'";
            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            if (data.Tables[0].Rows.Count == 0)
                return null;
            var picture = new RamadanBag(data.Tables[0].Rows[0]);
            picture.ID_FrontImagePath = picture.ID_FrontImagePath.Replace("\\", "/");
            picture.ID_RearImagePath = picture.ID_RearImagePath.Replace("\\", "/");
            return picture;
        }

        /// <summary>
        /// Gets all Pictures
        /// </summary>
        /// <param name="name">name; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>list of Pictures</returns>
        public virtual IPagedList<RamadanBag> GetAllRamadanBags(
            out string errorMessage,
            string username = null,
            string ID = null,
            string serial = null,
            int pageIndex = 0,
            int pageSize = int.MaxValue
            )
        {
            errorMessage = "";
            var entities = new List<RamadanBag>();

            var whereQuery = new StringBuilder();
            whereQuery = whereQuery.Append($" Where B.{RamadanBag.ID_NO} is not null ");


            if (!string.IsNullOrWhiteSpace(username))
                whereQuery = whereQuery.Append($" AND B.{RamadanBag.USERNAME} = '{username}'");
            
            if (!string.IsNullOrWhiteSpace(ID))
                whereQuery = whereQuery.Append($" AND B.{RamadanBag.ID_NO} = '{ID}'");
            
            if (!string.IsNullOrWhiteSpace(serial))
                whereQuery = whereQuery.Append($" AND B.{RamadanBag.SERIAL} = '{serial}'");

            string sqlQuery=$@"{SelectRamadanBages()} {whereQuery} ORDER BY B.REC_DATE DESC";
           

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                entities.Add(new RamadanBag(data.Tables[0].Rows[i]));


            return new PagedList<RamadanBag>(entities, pageIndex, pageSize);
        }

        #endregion

        #endregion
    }
}