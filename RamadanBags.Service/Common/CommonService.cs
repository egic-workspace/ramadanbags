﻿using EGIC.Core.Domain.Common;
using EGIC.Data.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace RamadanBags.Services.Common
{
    public class CommonService : ICommonService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;

        #endregion

        #region CTOR

        public CommonService(IServiceProvider service)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
        }

        #endregion

        #region Methods


        /// <summary>
        /// Gets or sets the current user permissions
        /// </summary>
        public virtual List<AppSetting> GetAppSettings()
        {
            string sql = "SELECT CODE,SETTING_NAME,SETTING_VALUE FROM APPS_SETTINGS WHERE APP_NAME = 'RamadanBags'";
            var data = _oracleAcess.ExecuteDataSet(sql);

            if (data.Tables[0].Rows.Count == 0)
                return new List<AppSetting>();

            List<AppSetting> _appSettings = new List<AppSetting>();
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                _appSettings.Add(new AppSetting(data.Tables[0].Rows[i]));

            return _appSettings;
        }

        public virtual List<SelectListItem> GetAllGovs(string selectedGovCode = "")
        {
            string sql = "SELECT CODE,NAME FROM GOVS";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "اختر المحافظة", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++) {
                var row = data.Tables[0].Rows[i];
                string code = row["CODE"].ToString();
                list.Add(new SelectListItem() { Text = row["NAME"].ToString(), Value = code, Selected = selectedGovCode == code });
            }
            return list;
        }

        public virtual List<SelectListItem> GetAllCities(string govCode, string selectedCityCode = "")
        {

            string sql = $"SELECT CODE,NAME FROM CITIES WHERE GOV_CODE = {govCode}";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "اختر المدينة", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string code = row["CODE"].ToString();
                list.Add(new SelectListItem() { Text = row["NAME"].ToString(), Value = code, Selected = selectedCityCode == code });
            }
            return list;
        }

        public virtual List<SelectListItem> GetShopGovs(string selectedGovCode = "")
        {

            string sql = $"SELECT CODE,NAME FROM SAL_GOVS";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "اختر المحافظة", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string code = row["CODE"].ToString();
                list.Add(new SelectListItem() { Text = row["NAME"].ToString(), Value = code, Selected = selectedGovCode == code });
            }
            return list;
        }

        public virtual List<SelectListItem> GetShopAreas(string govCode, string selectedAreaCode = "")
        {

            string sql = $"SELECT CODE,NAME FROM SAL_SUB_AREA WHERE MAIN_CODE IN(SELECT CODE FROM SAL_MAIN_AREA WHERE GOV_CODE = {govCode})";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "اختر المنطقة", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string code = row["CODE"].ToString();
                list.Add(new SelectListItem() { Text = row["NAME"].ToString(), Value = code, Selected = selectedAreaCode == code });
            }
            return list;
        }

        public virtual List<SelectListItem> GetAllShops(string areaCode, string selectedShopCode = "")
        {

            string sql = $"SELECT SAL_CODE,SHOP_NAME FROM SV_SHOPS WHERE RECORD_TYPE IN('EGIC','AdHoc','AdHoc2') AND SAL_SUB_AREA = { areaCode}";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "اختر المحل", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string code = row["SAL_CODE"].ToString();
                string name = $"{code} - {row["SHOP_NAME"]}";
                list.Add(new SelectListItem() { Text = name, Value = code, Selected = selectedShopCode == code });
            }
            return list;
        }

        #endregion
    }
}