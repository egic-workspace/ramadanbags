﻿using EGIC.Core.Domain.Common;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace RamadanBags.Services.Common
{
    public interface ICommonService
    {
        List<AppSetting> GetAppSettings();

        List<SelectListItem> GetAllGovs(string selectedGovCode = "");

        List<SelectListItem> GetAllCities(string govCode, string selectedCityCode = "");
        List<SelectListItem> GetShopGovs(string selectedGovCode = "");
        List<SelectListItem> GetShopAreas(string govCode, string selectedAreaCode = "");
        List<SelectListItem> GetAllShops(string areaCode, string selectedShopCode = "");
    }
}