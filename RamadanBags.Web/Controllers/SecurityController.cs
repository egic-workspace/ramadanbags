﻿using EGIC.Core.Domain.Users;
using EGIC.Core.Models.Security;
using EGIC.Core.Models.Users;
using Microsoft.AspNetCore.Mvc;
using RamadanBags.Services.Authentication;
using System;

namespace RamadanBags.Web.Controllers
{
    public class SecurityController : BaseController
    {
        #region Fields

        private readonly IAuthenticationService _authenticationService;

        #endregion

        #region CTOR

        public SecurityController(
            IServiceProvider service,
            IAuthenticationService authenticationService) : base(service)
        {
            _authenticationService = authenticationService;
        }

        #endregion

        #region Utilities


        #endregion

        #region Actions

        #region Login/Logout

        [Route("/login")]
        public IActionResult Login()
        {
            var model = new LoginModel();
            return View(model);
        }

        [HttpPost]
        [Route("/login")]
        public virtual IActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                model.Username = model.Username.Trim().ToUpper();
                var loginResult = _authenticationService.ValidateUser(model.Username, model.Password, out User user);
                switch (loginResult)
                {
                    case UserLoginResults.Successful:
                        {


                            //sign in new user
                            _authenticationService.SignIn(user);

                            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                                return RedirectToAction("Index", "Home");

                            return Redirect(returnUrl);
                        }
                    case UserLoginResults.UserNotExist:
                        ModelState.AddModelError("", "هذا المُستخدم غير موجود");
                        break;
                    case UserLoginResults.Deleted:
                        ModelState.AddModelError("", "تم ايقاف هذا المُستخدم");
                        break;
                    case UserLoginResults.WrongPassword:
                    default:
                        ModelState.AddModelError("", "من فضلك تأكد من كلمة المرور, وأعد المحاولة.");//
                        break;
                }
            }

            return View(model);
        }

        [Route("/logout")]
        public virtual IActionResult Logout()
        {
            //standard logout 
            _authenticationService.SignOut();

            return RedirectToAction("Login");
        }

        #endregion

        public virtual IActionResult AccessDenied(string pageUrl)
        {
            var currentUser = _workContext.CurrentUser;
            return View();
        }

        #endregion
    }
}