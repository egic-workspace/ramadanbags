﻿using EGIC.Core.Domain.RamadanBags;
using EGIC.Core.Models.RamadanBags;
using EGIC.Web.Framework;
using EGIC.Web.Framework.Extensions;
using EGIC.Web.Framework.Kendoui;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using RamadanBags.Services.Common;
using RamadanBags.Services.RamadanBags;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace RamadanBags.Web.Controllers
{
    [Route("RamadanBags")]
    public class RamadanBagsController : BaseController
    {

        #region Fields

        private readonly IRamadanBagService _ramadanBagsService;
        private readonly ICommonService _commonService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;

        #endregion

        #region Ctor

        public RamadanBagsController(IServiceProvider service,
            IRamadanBagService ramadanBagsService,
            ICommonService commonService, 
            IHostingEnvironment hostingEnvironment,
            IConfiguration config) : base(service)
        {
            _hostingEnvironment = hostingEnvironment;
            _ramadanBagsService = ramadanBagsService;
            _commonService = commonService;
            _config = config;
        }

        #endregion

        #region Utilities

        private void PrepareRamadanBagModel(ref RamadanBagModel model, RamadanBag bag)
        {
            if (bag != null)
            {
                model = new RamadanBagModel(bag);
                model.Govs = _commonService.GetAllGovs(bag.GovCode.ToString());
                model.Cities = _commonService.GetAllCities(bag.GovCode.ToString(), bag.CityCode.ToString());
                model.ShopGovs = _commonService.GetShopGovs();
                model.Shops = new List<SelectListItem> { new SelectListItem { Text = bag.ShopName, Value = bag.ShopCode.ToString() } };
                model.Editable = true;
            }
            else
            {
                model.Govs = _commonService.GetAllGovs($"{model.GovCode}");
                model.Cities = _commonService.GetAllCities($"{model.GovCode}", $"{model.CityCode}");
                model.ShopGovs = _commonService.GetShopGovs($"{model.ShopGovCode}");
                model.ShopAreas = _commonService.GetShopAreas($"{model.ShopGovCode}", $"{model.ShopAreaCode}");
                model.Shops = _commonService.GetAllShops($"{model.ShopAreaCode}", $"{model.ShopCode}");
            }
        }

        private bool UploadPicture(IFormFile httpPostedFile, out string errorMessage,out string imageUrl)
        {
            errorMessage = "";
            imageUrl = "";
            var fileBinary = httpPostedFile.GetDownloadBits();
            var qqFileNameParameter = "qqfilename";
            var fileName = httpPostedFile.FileName;
            if (string.IsNullOrEmpty(fileName) && Request.Form.ContainsKey(qqFileNameParameter))
                fileName = Request.Form[qqFileNameParameter].ToString();
            //remove path (passed in IE)
            fileName = Path.GetFileName(fileName);

            var contentType = httpPostedFile.ContentType;

            var fileExtension = Path.GetExtension(fileName);
            if (!string.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            var extensions = new List<string>() { ".bmp", ".gif", ".jpeg", ".jpg", ".jpe", ".png" };
            if (!extensions.Contains(fileExtension))
            {
                errorMessage = $"الرجاء اختيار صورة بأحد الامتدادات المسموح بها فقط كالتالي ({extensions.Aggregate((a, b) => a + ", " + b)}) .";
                return false;
            }

            //int defaultFileSizeKB = _config.GetValue<int>("AppSettings:PictureSettings:DefaultFileSizeKB");
            //if (httpPostedFile.Length > defaultFileSizeKB * 1024)
            //{
            //    errorMessage = $"حجم الملف المرفوع هو ({httpPostedFile.Length / 1024} كيلو بايت) وهذا الحجم اكبر من الحجم المسموح به وهو ({defaultFileSizeKB} كيلو بايت)";
            //    return false;
            //}

            //int width = 0, height = 0;
            //using (Stream memStream = new MemoryStream(fileBinary))
            //{
            //    using (Image img = Image.FromStream(memStream))
            //    {

            //        width = img.Width;
            //        height = img.Height;
            //    }
            //}
            //int systemMinWidth = _config.GetValue<int>("AppSettings:PictureSettings:DefaultSystemMinWidth"),
            //    systemMinHeight = _config.GetValue<int>("AppSettings:PictureSettings:DefaultSystemMinHeight"),

            //    systemMaxWidth = _config.GetValue<int>("AppSettings:PictureSettings:DefaultSystemMaxWidth"),
            //    systemMaxHeight = _config.GetValue<int>("AppSettings:PictureSettings:DefaultSystemMaxHeight");

            //if (width < systemMinWidth || width > systemMaxWidth || height < systemMinHeight || height > systemMaxHeight)
            //{
            //    errorMessage = $"ابعاد الصورة المرفوعة غير مطابقه, ابعاد الصورة يجب ان تكون بين  ({systemMinHeight},{systemMinWidth}) و ({systemMaxHeight},{systemMaxWidth}) ";
            //    return false;
            //}
            //contentType is not always available 
            //that's why we manually update it here
            //http://www.sfsu.edu/training/mimetype.htm
            if (string.IsNullOrEmpty(contentType))
            {
                switch (fileExtension)
                {
                    case ".bmp":
                        contentType = MimeTypes.ImageBmp;
                        break;
                    case ".gif":
                        contentType = MimeTypes.ImageGif;
                        break;
                    case ".jpeg":
                    case ".jpg":
                    case ".jpe":
                    case ".jfif":
                    case ".pjpeg":
                    case ".pjp":
                        contentType = MimeTypes.ImageJpeg;
                        break;
                    case ".png":
                        contentType = MimeTypes.ImagePng;
                        break;
                    case ".tiff":
                    case ".tif":
                        contentType = MimeTypes.ImageTiff;
                        break;
                    default:
                        break;
                }
            }

            if (string.IsNullOrWhiteSpace(errorMessage))
                imageUrl = _ramadanBagsService.SavePictureInFile(fileBinary);
            return true;
        }

        #endregion

        #region Actions

        #region Picture CRUD

        [Route("All")]
        public virtual IActionResult Index()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new RamadanBagListModel();
            return View(model);
        }

        [Route("RamadanBagsList"), HttpPost]
        public virtual IActionResult RamadanBagsList(DataSourceRequest command, RamadanBagListModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var data = _ramadanBagsService.GetAllRamadanBags(
                out string errorMassage,
                username: _workContext.CurrentUser.Username,
                ID: model.SearchID,
                serial: model.SearchSerial,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize
                );

            for (int i = 0; i < data.Count; i++)
                if (data[i].Username == _workContext.CurrentUser.Username)
                    data[i].Editable = true;

            var gridModel = new DataSourceResult
            {
                Data = data,
                Total = data.TotalCount
            };
            if (!string.IsNullOrWhiteSpace(errorMassage))
                return ErrorForKendoGridJson(errorMassage);
            return Json(gridModel);
        }

        [Route("GetCities"), HttpPost]
        public virtual IActionResult GetCities(string govCode)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();
            var data = _commonService.GetAllCities(govCode);
            return Json(new { success = true, data = data });
        }
        
        [Route("GetShopAreas"), HttpPost]
        public virtual IActionResult GetShopAreas(string govCode)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();
            var data = _commonService.GetShopAreas(govCode);
            return Json(new { success = true, data = data });
        }
        
        [Route("GetShops"), HttpPost]
        public virtual IActionResult GetShops(string areaCode)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();
            var data = _commonService.GetAllShops(areaCode);
            return Json(new { success = true, data = data });
        }

        [Route("Add")]
        public virtual IActionResult Add()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new RamadanBagModel();
            model.Govs = _commonService.GetAllGovs();
            model.ShopGovs = _commonService.GetShopGovs();
            return View(model);
        }

        [Route("Add"), HttpPost]
        public virtual IActionResult AddRamadanBag(RamadanBagModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();
            var bag = _ramadanBagsService.GetRamadanBagByID(model.ID);
            if(bag != null)
            {
                ErrorNotification($"تم تسليم شنطة رمضان من قبل للرقم القومي المدخل ({bag.ID})");
                PrepareRamadanBagModel(ref model, null);
                return View("Add", model);
            }

            bag = new RamadanBag(model);
            bag.Username = _workContext.CurrentUser.Username;
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(bag.ID_FrontImagePath) && bag.ID_FrontImagePath.StartsWith("data:image/"))
                {
                    string finalFileName = $"svfb_{bag.ID}_front.png";
                    SaveByteArrayAsImage(bag.ID_FrontImagePath, finalFileName);
                    bag.ID_FrontImagePath = $"/images/uploaded/{finalFileName}";
                }
                if (!string.IsNullOrWhiteSpace(bag.ID_RearImagePath) && bag.ID_RearImagePath.StartsWith("data:image/"))
                {
                    string finalFileName = $"svfb_{bag.ID}_rear.png";
                    SaveByteArrayAsImage(bag.ID_RearImagePath, finalFileName);
                    bag.ID_RearImagePath = $"/images/uploaded/{finalFileName}";
                }
                _ramadanBagsService.InsertRamadanBag(bag);


                SuccessNotification($"تمت الإضافة بنجاح");
                return RedirectToAction("Index");
            }
            PrepareRamadanBagModel(ref model, bag);
            return View("Add", model);
        }
        private void SaveByteArrayAsImage(string base64String,string finalFileName)
        {
            base64String = base64String.Replace("data:image/gif;base64,", "");
            base64String = base64String.Replace("data:image/png;base64,", "");
            base64String = base64String.Replace("data:image/jpg;base64,", "");
            base64String = base64String.Replace("data:image/jpeg;base64,", "");
            byte[] bytes = Convert.FromBase64String(base64String);
            string fullOutputPath = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploaded", finalFileName);

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                Image image = Image.FromStream(ms);
                using (FileStream fs = new FileStream(fullOutputPath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    byte[] bytesOut = ms.ToArray();
                    fs.Write(bytesOut, 0, bytesOut.Length);
                }
            }
        }

        //AddPicture
        //do not validate request token (XSRF)
        [HttpPost]
        [IgnoreAntiforgeryToken]
        [Route("AsyncUploadPicture")]
        public virtual IActionResult AsyncUploadPicture()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var httpPostedFile = Request.Form.Files.FirstOrDefault();
            if (httpPostedFile == null)
            {
                return Json(new
                {
                    success = false,
                    message = "There is no file to upload",
                    downloadGuid = Guid.Empty,
                });
            }

            
            bool success = UploadPicture(httpPostedFile,out string errorMessage, out string imageUrl);
           
            return Json(new
            {
                success = success,
                message = errorMessage,
                imageUrl = imageUrl
            });
        }

        [Route("{ID}/Update")]
        [Route("Update/{ID}")]
        public virtual IActionResult Update(string ID)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var bag = _ramadanBagsService.GetRamadanBagByID(ID);
            if (bag == null)
            {
                ErrorNotification($"لا يوجد شنط مُدخلة لهذا الرقم القومي ({ID})");
                return RedirectToAction("Index");
            }
            var model = new RamadanBagModel();
            PrepareRamadanBagModel(ref model, bag);
            return View(model);
        }

        [Route("SaveRamadanBag"), HttpPost]
        public virtual IActionResult SaveRamadanBag(RamadanBagModel model, IFormCollection form)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var bag = _ramadanBagsService.GetRamadanBagByID(model.ID);
            if(bag == null)
            {
                WarningNotification($"خطأ في ايجاد شنطة رمضان خاصه بهذا الرقم القومي {model.ID} ");
                return RedirectToAction("Index");
            }

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(model.ID_FrontImagePath) && model.ID_FrontImagePath.StartsWith("data:image/"))
                {
                    string finalFileName = $"svfb_{model.ID}_front.png";
                    SaveByteArrayAsImage(model.ID_FrontImagePath, finalFileName);
                    model.ID_FrontImagePath = $"/images/uploaded/{finalFileName}";
                }
                if (!string.IsNullOrWhiteSpace(model.ID_RearImagePath) && model.ID_RearImagePath.StartsWith("data:image/"))
                {
                    string finalFileName = $"svfb_{model.ID}_rear.png";
                    SaveByteArrayAsImage(model.ID_RearImagePath, finalFileName);
                    model.ID_RearImagePath = $"/images/uploaded/{finalFileName}";
                }
                bag = new RamadanBag(model);
                _ramadanBagsService.SaveRamadanBag(bag);
                SuccessNotification($"تم تعديل البيانات بنجاح");
                return RedirectToAction("Index");
            }
            return RedirectToAction("Update", new { ID = model.ID });
        }

        [Route("{code}/Delete")]
        [Route("Delete/{code}")]
        public virtual IActionResult Delete(string ID, bool isAjax = false)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var bag = _ramadanBagsService.GetRamadanBagByID(ID);
            if (bag == null)
                return RedirectToAction("Index");

            try
            {
                //delete
                bool success = _ramadanBagsService.DeleteRamadanBag(bag);
                if (success)
                {
                    if (isAjax)
                        return Json(new { success = true });

                    SuccessNotification($"تم الحذف بنجاح");
                }
                else
                {
                    string errorMessage = "تعذر الحذف";
                    if (isAjax)
                        return Json(new { success = false, errorMessage = errorMessage });
                    ErrorNotification(errorMessage);
                    return RedirectToAction("Update", new { code = ID });
                }
                return RedirectToAction("Index");
            }
            catch (Exception exc)
            {
                if (isAjax)
                    return Json(new { success = false, errorMessage = exc.Message });
                ErrorNotification(exc.Message);
                return RedirectToAction("Index");
            }
        }

        #endregion

        #endregion

        #region picture

        #endregion
    }
}