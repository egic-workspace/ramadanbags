﻿using EGIC.Core.Domain.RamadanBags;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.RamadanBags
{
    public partial class RamadanBagModel : RamadanBag
    {
        public RamadanBagModel() : base()
        {
            Shops = new List<SelectListItem>();
            Govs = new List<SelectListItem>();
            Cities = new List<SelectListItem>();
            ShopGovs = new List<SelectListItem>();
            ShopAreas = new List<SelectListItem>();
            Pictures = new PicturesModel();
        }

        public RamadanBagModel(RamadanBag bag) : this()
        {
            Name = bag.Name;
            ID_FrontImagePath = bag.ID_FrontImagePath;
            ID_RearImagePath = bag.ID_RearImagePath;
            ID = bag.ID;
            ShopCode = bag.ShopCode;
            ShopName = bag.ShopName;
            Job = bag.Job;
            Mobile = bag.Mobile;
            Serial = bag.Serial;
            CityCode = bag.CityCode;
            GovCode = bag.GovCode;
            FamilyCount = bag.FamilyCount;
            Username = bag.Username;
            CreatedOn = bag.CreatedOn;
        }

        public PicturesModel Pictures { get; set; }

        public int? ShopAreaCode { get; set; }
        public int? ShopGovCode { get; set; }

        public List<SelectListItem> Shops { get; set; }
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> Govs { get; set; }
        public List<SelectListItem> ShopAreas { get; set; }
        public List<SelectListItem> ShopGovs { get; set; }

    }
}