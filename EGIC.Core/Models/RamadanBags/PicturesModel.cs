﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EGIC.Core.Models.RamadanBags
{
    public class PicturesModel
    {

        #region Properties

        /// <summary>
        /// Gets or sets the picture code
        /// </summary>
        [EGICResourceDisplayName("الصور")]
        [UIHint("Pictures")]
        public string PicturesCodes { get; set; }

        /// <summary>
        /// parent model pictures codes property to update it with uploaded pictures codes
        /// </summary>
        public string ParentModelPicturesCodesProp { get; set; }

        /// <summary>
        /// Gets or sets the entity code related to this picture
        /// </summary>
        public int EntityCode { get; set; }

        /// <summary>
        /// button id to bind upload images on click
        /// </summary>
        public string UploadButttonID { get; set; }

        #endregion
    }
}
