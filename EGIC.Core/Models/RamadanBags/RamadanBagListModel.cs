﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;

namespace EGIC.Core.Models.RamadanBags
{
    public partial class RamadanBagListModel
    {
        public RamadanBagListModel()
        {
        }


        [EGICResourceDisplayName("رقم الكشف")]
        public string SearchSerial { get; set; }

        [EGICResourceDisplayName("الرقم القومي")]
        public string SearchID { get; set; }

    }
}