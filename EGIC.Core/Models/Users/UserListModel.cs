﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.Users
{
    public partial class UserListModel
    {
        public UserListModel()
        {
            AvailableUserRoles = new List<SelectListItem>();
            SearchUserRoleCodes = new List<int>();
        }

        [EGICResourceDisplayName("Code")]
        public int SearchCode { get; set; }

        [EGICResourceDisplayName("Username")]
        public string SearchUsername { get; set; }

        [EGICResourceDisplayName("Full name")]
        public string SearchFullname { get; set; }

        [EGICResourceDisplayName("Roles")]
        public List<int> SearchUserRoleCodes { get; set; }
        public List<SelectListItem> AvailableUserRoles { get; set; }
    }
}
