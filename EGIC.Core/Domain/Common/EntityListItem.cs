﻿using System.Data;

namespace EGIC.Core.Domain.Common
{
    public partial class EntityListItem
    {
        public EntityListItem()
        {
        }

        public EntityListItem(DataRow row) : this()
        {
            Text = row["TEXT"].ToString();
            Value = row["VALUE"].ToString();
        }
        public string Text { get; set; }
        public string Value { get; set; }
        public string ParentCode { get; set; }
    }
}
