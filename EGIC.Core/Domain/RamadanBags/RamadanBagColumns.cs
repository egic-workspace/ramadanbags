﻿namespace EGIC.Core.Domain.RamadanBags
{
    public partial class RamadanBag
    {
        public const string SAL_CODE = nameof(SAL_CODE);
        public const string SHOP_NAME = nameof(SHOP_NAME);

        public const string ID_NO = nameof(ID_NO);
        public const string BENF_NAME = nameof(BENF_NAME);
        public const string MOBILE = nameof(MOBILE);
        public const string JOB = nameof(JOB);
        public const string FAMILY = nameof(FAMILY);
        public const string GOV_CODE = nameof(GOV_CODE);
        public const string GOV_NAME = nameof(GOV_NAME);
        public const string CITY_CODE = nameof(CITY_CODE);
        public const string CITY_NAME = nameof(CITY_NAME);

        public const string IMG_PATH1 = nameof(IMG_PATH1);
        public const string IMG_PATH2 = nameof(IMG_PATH2);

        public const string SERIAL = nameof(SERIAL);
        public const string USERNAME = nameof(USERNAME);
        public const string REC_DATE = nameof(REC_DATE);
    }
}