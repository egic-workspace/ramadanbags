﻿using EGIC.Core.Models.RamadanBags;
using EGIC.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace EGIC.Core.Domain.RamadanBags
{
    public partial class RamadanBag : BaseEGICEntity
    {
        #region CTOR

        public RamadanBag() : base()
        {
        }

        public RamadanBag(DataRow row) : base(row) 
        {
            try
            {
                if (byte.TryParse(row[FAMILY].ToString(), out byte familyCount))
                    FamilyCount = familyCount;

                if (int.TryParse(row[GOV_CODE].ToString(), out int govCode))
                    GovCode = govCode;
                
                if (int.TryParse(row[CITY_CODE].ToString(), out int cityCode))
                    CityCode = cityCode;

                if (int.TryParse(row[SAL_CODE].ToString(), out int shopCode))
                    ShopCode = shopCode;

                if (DateTime.TryParse(row[REC_DATE].ToString(), out DateTime creationDate))
                    CreatedOn = creationDate;

                Name = row[BENF_NAME].ToString();
                Mobile = row[MOBILE].ToString();
                Job = row[JOB].ToString();
                ID_FrontImagePath = row[IMG_PATH1].ToString();
                ID_RearImagePath = row[IMG_PATH2].ToString();
                ID = row[ID_NO].ToString();
                Serial = row[SERIAL].ToString();
                Username = row[USERNAME].ToString();

                string name = $"{row[SAL_CODE]} - {row[SHOP_NAME]}- {row["SUB_AREA"]}";
                ShopName = name;
            }
            catch { }
        }

        public RamadanBag(RamadanBagModel bag) : this()
        {
            Name = bag.Name;
            ID_FrontImagePath = bag.ID_FrontImagePath;
            ID_RearImagePath = bag.ID_RearImagePath;
            ID = bag.ID;
            ShopCode = bag.ShopCode;
            ShopName = bag.ShopName;
            Job = bag.Job;
            Mobile = bag.Mobile;
            Serial = bag.Serial;
            CityCode = bag.CityCode;
            GovCode = bag.GovCode;
            FamilyCount = bag.FamilyCount;
            Username = bag.Username;
            CreatedOn = bag.CreatedOn;
        }
        #endregion

        #region Properties

        [EGICResourceDisplayName("اضيفت بواسطة")]
        public string Username { get; set; }

        [EGICResourceDisplayName("اضيفت بتاريخ")]
        public DateTime CreatedOn { get; set; }

        [EGICResourceDisplayName("اسم المستفيد")]
        [Required(ErrorMessage = "يجب ادخال اسم المستفيد")]
        public string Name { get; set; }

        [EGICResourceDisplayName("الموبايل")]
        [RegularExpression("[0-9]{11}",ErrorMessage = "رقم الموبايل يجب ان يكون 11 رقماً بالارقام الانجليزيه فقط")]
        public string Mobile { get; set; }

        [EGICResourceDisplayName("المهنة")]
        public string Job { get; set; }

        [EGICResourceDisplayName("عدد افراد الاسرة")]
        [Range(1,20,ErrorMessage = "يجب ان يكون عدد افراد الاسرة فرد او اكثر الي {1} فرد ")]
        public int FamilyCount { get; set; }

        [EGICResourceDisplayName("صورة البطاقة امامي")]
        //[UIHint("Picture")]
        [DataType(DataType.Upload)]
        public string ID_FrontImagePath { get; set; }

        [EGICResourceDisplayName("صورة البطاقة خلفي")]
        //[UIHint("Picture")]
        [DataType(DataType.Upload)]
        public string ID_RearImagePath { get; set; }

        [EGICResourceDisplayName("التاجر")]
        public string ShopName { get; set; }


        [EGICResourceDisplayName("التاجر")]
        [Required(ErrorMessage = "يجب تحديد التاجر")]
        public int ShopCode { get; set; }

        [EGICResourceDisplayName("المحافظة")]
        public int? GovCode { get; set; }

        [EGICResourceDisplayName("المنطقة")]
        public int? CityCode { get; set; }

        [EGICResourceDisplayName("الرقم القومي")]
        [RegularExpression("[0-9]{14}", ErrorMessage = "الرقم القومي يجب ان يكون 14 رقماً بالارقام الانجليزيه فقط")]
        [Required(AllowEmptyStrings = false,ErrorMessage = "يجب ادخال الرقم القومي")]
        public string ID { get; set; }

        [EGICResourceDisplayName("رقم الكشف")]
        [RegularExpression("[0-9]+", ErrorMessage = "رقم الكشف يجب ان يكون بالارقام الانجليزيه فقط")]
        [Required(ErrorMessage = "يجب ادخال رقم الكشف")]
        public string Serial { get; set; }

        public bool Editable { get; set; }
        #endregion
    }
}