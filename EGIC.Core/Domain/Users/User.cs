﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;

namespace EGIC.Core.Domain.Users
{
    public partial class User : BaseEGICEntity
    {
        public User()
        {
        }

        #region CTOR

        public User(DataRow row) : base(row)
        {
            try
            {
                FullName = row[FULL_NAME].ToString();
                Username = row[USER_NAME].ToString();
                Password = row[PASSWORD].ToString();
            }
            catch { }
        }

        #endregion

        #region Properties

        [EGICResourceDisplayName("الاسم")]
        public string FullName { get; set; }

        [EGICResourceDisplayName("اسم المٌستخدم")]
        [Required(ErrorMessage = "يجب ادخال اسم المٌستخدم")]
        public string Username { get; set; }

        [EGICResourceDisplayName("كلمة المرور")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "يجب ادخال كلمة المرور")]
        public string Password { get; set; }

        #endregion

    }
}
