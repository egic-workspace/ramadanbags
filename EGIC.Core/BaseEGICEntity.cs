﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using System.Data;

namespace EGIC.Core
{
    /// <summary>
    /// Represents base nopCommerce entity
    /// </summary>
    public partial class BaseEGICEntity
    {

        #region const

        public const string CODE = nameof(CODE);

        public const string DATE_FORMAT_ORECAL = "yyyy/mm/dd hh24:mi";
        public const string DATE_FORMAT_NET = "yyyy/MM/dd HH:mm";

        #endregion

        #region ctor

        public BaseEGICEntity()
        {
        }

        public BaseEGICEntity(DataRow row) : this()
        {
            try
            {
                if (int.TryParse(row[CODE].ToString(), out int _code))
                    Code = _code;
            }
            catch { }
        }

        #endregion

        /// <summary>
        /// Gets or sets model identifier
        /// </summary>
        [EGICResourceDisplayName("Code")]
        public virtual int Code { get; set; }

    }
}
